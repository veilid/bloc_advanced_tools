# Collection of useful tools for BLoC and Cubit development in Dart

Tests are not complete

## Contains

* AsyncTransformerCubit - Wrapper cubit that takes an input cubit and asynchronously transforms its state and produces a transformed output state.
* BlocBusyWrapper - Wrapper mixin adds 'busy state' processing to an input bloc or cubit, so you can tell if a cubit is performing a state-modifying operation.
* BlocMapCubit - Streaming container cubit that is a map from some immutable key to a some other cubit's output state.
* BlocToolsExtension - Extension methods to make common operations on blocs easier.
* FutureCubit - Simple base class for a cubit that initialized based on the result of a Future.
* StateMapFollower - Mixin that automatically keeps two blocs/cubits in sync with each other. Useful for having a BlocMapCubit 'follow' the state of another input cubit.
* StreamWrapperCubit - Simple base class to wrap a stream of states with a Cubit.
* TransformerCubit - Wrapper cubit that takes an input cubit and transforms its state and produces a transformed output state.
