import 'dart:async';

import 'package:bloc/bloc.dart';

/// A wrapper cubit that applies a state transformer function to all input
/// states of the wrapped cubit to produce a transformed state
class TransformerCubit<T, S, C extends Cubit<S>> extends Cubit<T> {
  TransformerCubit(this.input, {required this.transform})
      : super(transform(input.state)) {
    _subscription = input.stream.listen((event) => emit(transform(event)));
  }

  @override
  Future<void> close() async {
    await _subscription.cancel();
    await input.close();
    await super.close();
  }

  C input;
  T Function(S) transform;
  late final StreamSubscription<S> _subscription;
}
