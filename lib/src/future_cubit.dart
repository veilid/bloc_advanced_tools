import 'dart:async';

import 'package:async_tools/async_tools.dart';
import 'package:bloc/bloc.dart';

/// Simple base class for a cubit that initialized based on the result
/// of a Future.
abstract class FutureCubit<State> extends Cubit<AsyncValue<State>> {
  FutureCubit(Future<State> fut) : super(const AsyncValue.loading()) {
    _initWait.add((_) async => fut.then((value) {
          emit(AsyncValue.data(value));
          // ignore: avoid_types_on_closure_parameters
        }, onError: (Object e, StackTrace st) {
          addError(e, st);
          emit(AsyncValue.error(e, st));
        }));
  }
  FutureCubit.value(State state) : super(AsyncValue.data(state));

  @override
  Future<void> close() async {
    await _initWait();
    await super.close();
  }

  final WaitSet<void, void> _initWait = WaitSet();
}
