import 'package:async_tools/async_tools.dart';
import 'package:bloc/bloc.dart';

class BlocUpdater<B extends BlocBase<dynamic>, A> {
  BlocUpdater({required B Function(A) create}) : _create = create;

  B? update(A? arguments) {
    if (arguments == _lastArguments) {
      return bloc;
    }
    final closeBloc = bloc;
    bloc = null;
    if (closeBloc != null) {
      serialFuture(this, closeBloc.close);
    }
    if (arguments != null) {
      bloc = _create(arguments);
    }
    _lastArguments = arguments;
    return bloc;
  }

  Future<void> close() async {
    await serialFutureClose(this);
    final closeBloc = bloc;
    bloc = null;
    if (closeBloc != null) {
      await closeBloc.close();
    }
    _lastArguments = null;
  }

  final B Function(A) _create;
  B? bloc;
  A? _lastArguments;
}
