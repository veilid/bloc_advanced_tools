## 0.1.8
- update async_tools to 0.1.7

## 0.1.7
- add update() to BlocMapCubit
- switch BlocUpdater to use serialFutureClose to avoid dangling locks

## 0.1.6
- Improve error reporting for bloc state errors

## 0.1.5
- Update async_tools to 0.1.5

## 0.1.4
- Update async_tools to 0.1.4

## 0.1.3

- Make TransformerCubit specific to input cubit type

## 0.1.2

- Make syncFollowers work recursively down the follower chain

## 0.1.1

- Update async_tools to 0.1.1

## 0.1.0

- Initial version
